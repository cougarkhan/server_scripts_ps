#$GLFile = Read-Host "Enter location of GL File"

$Folder =  gci Q:\ChargeBack

foreach ($File in $Folder | Where-Object {$_.Name -match "glmpc"}) {
    $NewFileName = $File.FullName
    $NewFileName = $NewFileName.Replace("txt","dat")
    
    if (Test-Path $NewFileName) {
        Write-Host "`n$NewFileName ALREADY EXISTS!" -foregroundcolor "RED"
        exit
    } else {
        Write-Host "Reading from file..." $File.FullName
        $TempFile = gc $File.FullName
    
        for ($x=0; $x -lt $TempFile.Count; $x++) {
            if ($TempFile[$x] -match "INTERNAL") {
                $TempFile[$x] = $TempFile[$x].Replace("INTERNAL","                                                                                                                                                                                                                                                            INTERNAL")
                Write-Host "Made correction at line " $x
            }
        }

            
        $Part1 = $NewFileName.Substring(0,($NewFileName.Length - 6))
        $Part2 = $NewFileName.Substring($NewFileName.Length - 6)

        $NewFileName = $Part1 + "-" + $Part2

        Write-Host "Created new file..." $NewFileName

        Write-Host "Writing to file..." $NewFileName


        $TempFile | sc $NewFileName
        
        Write-Host Copying $File.FullName to "Q:\ChargeBack\archive_finance_loads"
        Copy-Item $File.Fullname -destination "Q:\ChargeBack\archive_finance_loads"
        
        Write-Host Copying $File.FullName to "Q:\ChargeBack\archive_finance_loads"
        Copy-Item $NewFileName -destination "Q:\ChargeBack\archive_finance_loads"
        

        Write-Host "`nDone!"
	$wait = Read-Host
    }
}