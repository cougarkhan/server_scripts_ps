﻿##############################################################################################
#
# Script to restart Cs.exe.exe when Cs.exe hangs.
#
##############################################################################################
#
# This script will check 1 condition.
# 
# 1. It will check whether the process named Cs.exe has stopped responding.  
#    If it has then it will kill Cs.exe.exe and restart Cs.exe which will 
#    automatically restart dsxpw.exe.
#
#
##############################################################################################
#
# Script written by Michael Pal on Nov 2nd 2011
#
##############################################################################################

# Set Error Action to STOP
$ErrorActionPreference = "STOP"

# Notification Recipients
$recipients = "michael.pal@ubc.ca,calvin.lo@ubc.ca,adil.leghari@ubc.ca,alarms@mail.ubc.ca"

# Import send email and date time functions
 . .\functions\send_email.ps1
 . .\functions\format_date.ps1

# Array Variable for storing error message
[array]$errorLog = @{}

# funciton to kill processes
function killProcess ($process) {
	if ($process) {
		$process.kill()
	}
}

try {
	$cs = gps Cs
} catch {
	$errDate = (Get-Date).toshorttimestring()
	$errorLog = "Process Cs.exe has stopped responding at approximately $errDate`n"
	
	killProcess($cs)
	
    & "C:\WinDSX\Cs.exe" "-visible"
	$errorLog += "`nRestarted Process Cs.exe at " + (Get-Date).toshorttimestring() +"`n"
	$errorLog += "`nGood Hunting!"
	
	send_email $errorLog $recipients
}

# If the Responding property for Cs.exe is $false then kill the processes and restart them
# if ($cs.Responding -eq $false) {
	# $errDate = (Get-Date).toshorttimestring()
	# $errorLog = "Process Cs.exe has stopped responding at approximately $errDate`n"
	
	# killProcess($cs)
	# $errorLog += "`nKilled Process Cs.exe at " + (Get-Date).toshorttimestring() +"`n"
	
	# sleep 5
	# & "D:\WinDSX\Cs.exe" "-visible"
	# $errorLog += "`nRestarted Process Cs.exe at " + (Get-Date).toshorttimestring() +"`n"
	# $errorLog += "`nGood Hunting!"
	
	# send_email($errorLog)
# }