$winscp_exe = "C:\Program Files\WinSCP\WinSCP.exe"

$winscp_commands = @(
"/console",
"/script=get_card_data.txt",
"/log=C:\DRS\Logs\carddata.log"
"/privatekey=C:\DRS\Keys\bis-appscan-prd-prv"
)

start-process -filepath $winscp_exe -argumentlist $winscp_commands -wait

cp E:\CardData.bak\* E:\CardData\ -verbose -filter *.trn -recurse
ls E:\CardData.bak -filter *.trn | rename-item -newname { $_.name -replace '.trn','.csv' }
ls E:\CardData -filter *.trn | rename-item -newname { $_.name -replace '.trn','.csv' }

# foreach ($second in 90..0) {
	# write-host "`rSleeping for $second..." -nonewline
	# sleep -seconds 1
# }

# if (! (ps ScanNetBackup -ea silentlycontinue) ) {

	# $conn = new-object System.Data.Odbc.OdbcConnection
	# $cmd = new-object System.Data.Odbc.OdbcCommand

	# $conn.ConnectionString = "Driver=Adaptive Server Anywhere 9.0;ENG=bis-appscan-prd_db_server;UID=scan;PWD=scan; DBN=sa_scan;LINKS=TCPIP(HOST=137.82.155.170);"
	# $conn.Open()
	# $cmd.Connection = $conn


	# $update_query = @(
	# "UPDATE `"scan`".`"table_acs_credential`" SET Pass_Back='1'",
	# "UPDATE `"scan`".`"table_acs_credential`" SET Pass_Back='0' where DBAorGroup_ID='3'",
	# "UPDATE `"scan`".`"table_acs_credential`" SET Pass_Back='0' where DBAorGroup_ID='5'",
	# "UPDATE `"scan`".`"table_acs_credential`" SET Pass_Back='0' where DBAorGroup_ID='13'"
	# )

	# foreach ($update in $update_query) {
		# write-host "Executing   $update"
		# $cmd.CommandText = $update
		# $result = $cmd.ExecuteNonQuery()
		# write-host "$result rows effected."
		# $cmd.CommandText = ""
	# }

	# $cmd.CommandText = ""
	# $cmd.Connection = $null
	# $cmd = $null
	# $conn.close()
	# $conn = $null
# }
